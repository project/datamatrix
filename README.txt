
Data Matrix API for Drupal
==========================
libdmtx is an open-source library for reading and writing Data Matrix
barcodes: two-dimensional symbols that hold a dense pattern of data with
built-in error correction.

This Drupal 6.x-compatible module provides a PHP wrapper API for accessing
libdmtx's encoding/decoding functionality. It does not contain end-user
functionality per se, and should only be installed where another module
requires it as a dependency. For more information please see:

  <http://drupal.org/project/datamatrix>

For more information about libdmtx and Data Matrix barcodes in general,
please refer to:

  <http://www.libdmtx.org/>
  <http://en.wikipedia.org/wiki/Data_matrix_(computer)>
  <http://mobilecodes.nokia.com/learn.htm>


BUG REPORTS
-----------
Post bug reports and feature requests to the issue tracking system at:

  <http://drupal.org/node/add/project-issue/datamatrix>


FEATURE REQUESTS
----------------
The author is available for contract development and customization relating
to this module. You can reach him at <http://drupal.org/user/26089/contact>.


CREDITS
-------
Developed and maintained by Arto Bendiken <http://ar.to/>
Inspired by Vinay Gupta <http://guptaoption.com/>
Developed for the Hexayurt Project <http://hexayurt.com/>
Kudos to Mike Laughton <http://www.ohloh.net/accounts/mblaughton>
