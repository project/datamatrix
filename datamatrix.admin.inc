<?php

//////////////////////////////////////////////////////////////////////////////
// Settings form

function datamatrix_admin_settings() {
  $form = array();

  // libdmtx settings
  $form['libdmtx'] = array('#type' => 'fieldset', '#title' => t('libdmtx settings'), '#collapsible' => TRUE, '#collapsed' => FALSE);
  $form['libdmtx']['datamatrix_dmtxwrite'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Path to encoder'),
    '#default_value' => DATAMATRIX_DMTXWRITE,
    '#size'          => 60,
    '#maxlength'     => 255,
    '#required'      => TRUE,
    '#description'   => t('A file system path to the <code>dmtxwrite</code> binary. On Unix systems, this would typically be located at <code>/usr/bin/dmtxwrite</code> or <code>/usr/local/bin/dmtxwrite</code>. On Mac OS X with MacPorts, the path would typically be <code>/opt/local/bin/dmtxwrite</code>.'),
  );
  $form['libdmtx']['datamatrix_dmtxread'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Path to decoder'),
    '#default_value' => DATAMATRIX_DMTXREAD,
    '#size'          => 60,
    '#maxlength'     => 255,
    '#required'      => TRUE,
    '#description'   => t('A file system path to the <code>dmtxread</code> binary. On Unix systems, this would typically be located at <code>/usr/bin/dmtxread</code> or <code>/usr/local/bin/dmtxread</code>. On Mac OS X with MacPorts, the path would typically be <code>/opt/local/bin/dmtxread</code>.'),
  );

  return system_settings_form($form);
}

function datamatrix_admin_settings_validate($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  if (!file_exists($datamatrix_dmtxwrite)) {
    form_set_error('datamatrix_dmtxwrite', t('The encoder path is invalid: %path not found.', array('%path' => $datamatrix_dmtxwrite)));
  }

  if (!file_exists($datamatrix_dmtxread)) {
    form_set_error('datamatrix_dmtxread', t('The decoder path is invalid: %path not found.', array('%path' => $datamatrix_dmtxread)));
  }
}
